package to2.dziekanat.util;

import com.vaadin.data.Property;
import com.vaadin.ui.ComboBox;

public class ComboBoxCreator extends AbstractComponentCreator<ComboBox, ComboBoxCreator> {

    private final ComboBox comboBox;

    public ComboBoxCreator() {
        this.comboBox = new ComboBox();
    }

    public ComboBoxCreator onValueChange(Property.ValueChangeListener listener) {
        comboBox.addValueChangeListener(listener);
        return this;
    }

    public ComboBoxCreator addItem(Object o) {
        comboBox.addItem(o);
        return this;
    }

    public ComboBoxCreator value(Object o) {
        comboBox.setValue(o);
        return this;
    }

    public ComboBoxCreator enabled(boolean enabled) {
        comboBox.setEnabled(enabled);
        return this;
    }

    public ComboBoxCreator nullSelection(boolean allowed) {
        comboBox.setNullSelectionAllowed(allowed);
        return this;
    }

    @Override
    public ComboBox get() {
        return comboBox;
    }

    @Override
    public ComboBoxCreator instance() {
        return this;
    }
}
