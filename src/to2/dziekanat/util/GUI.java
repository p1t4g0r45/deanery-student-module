package to2.dziekanat.util;

public class GUI {
    private GUI() {
    }

    public static ButtonComponentCreator button() {
        return new ButtonComponentCreator();
    }

    public static LabelComponentCreator label() {
        return new LabelComponentCreator();
    }

    public static VerticalLayoutCreator vertical() {
        return new VerticalLayoutCreator();
    }

    public static HorizontalLayoutCreator horizontal() {
        return new HorizontalLayoutCreator();
    }

    public static PanelCreator panel() {
        return new PanelCreator();
    }

    public static TextFieldCreator textfield() {
        return new TextFieldCreator();
    }

    public static ComboBoxCreator combobox() {
        return new ComboBoxCreator();
    }

    public static TextAreaCreator textarea() {
        return new TextAreaCreator();
    }
}
