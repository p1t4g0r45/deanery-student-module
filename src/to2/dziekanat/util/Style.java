package to2.dziekanat.util;

public class Style {
    public static final String ULTRA_LIGHT_GRAY = "backgroundUltraLightGray";
    public static final String LIGHT_GRAY = "backgroundLightGray";
    public static final String PADDING10 = "paddingTen";
    public static final String MARGIN10 = "marginTen";
    public static final String DEFAULT_BGCOLOUR = "defaultBGcolour";
}
