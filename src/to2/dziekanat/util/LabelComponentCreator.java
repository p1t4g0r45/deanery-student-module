package to2.dziekanat.util;

import com.vaadin.ui.Label;

public class LabelComponentCreator extends AbstractComponentCreator<Label, LabelComponentCreator> {

    private final Label label;

    public LabelComponentCreator() {
        this.label = new Label();
    }

    public LabelComponentCreator value(String value) {
        label.setValue(value);
        return this;
    }

    @Override
    public Label get() {
        return label;
    }

    @Override
    public LabelComponentCreator instance() {
        return this;
    }

}
