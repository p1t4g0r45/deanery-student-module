package to2.dziekanat.util;

import com.vaadin.ui.HorizontalLayout;

public class HorizontalLayoutCreator extends AbstractLayoutCreator<HorizontalLayout, HorizontalLayoutCreator> {

    private final HorizontalLayout layout;

    public HorizontalLayoutCreator() {
        this.layout = new HorizontalLayout();
    }

    @Override
    public HorizontalLayout get() {
        return layout;
    }

    @Override
    public HorizontalLayoutCreator instance() {
        return this;
    }
}
