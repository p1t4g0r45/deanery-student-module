package to2.dziekanat;

import com.google.common.collect.Sets;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import to2.dziekanat.model.*;
import to2.dziekanat.persistence.HibernateUtils;
import to2.dziekanat.view.OverallView;
import to2.dziekanat.view.login.LogInView;
import to2.dziekanat.view.login.LoginListener;

import javax.servlet.annotation.WebServlet;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@SuppressWarnings("serial")
@Theme("wd")
public class WdUI extends UI implements LoginListener {

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = WdUI.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {

        HibernateUtils.getSessionFactory().openSession().close();


        //populateDB();


        Object user = UI.getCurrent().getSession().getAttribute("user");
        if (user == null) {
            setContent(new LogInView(this));
        } else {
            setContent(new OverallView());
        }

    }


    @Override
    public void onLogin(String id, String password) {

        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            List<UserAccount> list = session.createCriteria(UserAccount.class).add(Restrictions.eq("studentIdNumber", id)).list();
            Optional<UserAccount> first = list.stream().findFirst();
            if (first.isPresent() && Objects.equals(first.get().getPassword(), password)) {
                List<Student> students = session.createCriteria(Student.class).add(Restrictions.eq("studentIdNumber", id)).list();
                Optional<Student> student = students.stream().findFirst();
                if (student.isPresent()) {
                    UI.getCurrent().getSession().setAttribute("user", student.get());
                    Notification.show("Witaj " + student.get().getFullName());
                    setContent(new OverallView());
                    return;
                }
            }
            Notification.show("Niepoprawne dane", Notification.Type.WARNING_MESSAGE);
        }
    }

    private void populateDB() {
        LocalDate localDate = LocalDate.now();
        LocalDateTime dateTime = LocalDateTime.now();
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();
        Teacher teacher = new Teacher("Waclaw", "Frydrych", "Dr");
        session.save(teacher);
        Year year = new Year(2);
        session.save(year);
        Semester semester = new Semester(null, 1, localDate, localDate, localDate);
        session.save(semester);
        Course course = new Course("matematyka", semester, teacher);
        session.save(course);
        CourseComponent component = new CourseComponent(course, CourseComponentType.LAB_CLASS);
        session.save(component);
        StudentGroup group = new StudentGroup("grupa A", component, teacher, null);
        session.save(group);
        StudentGroup group2 = new StudentGroup("grupa wykladowa", component, teacher, null);
        session.save(group2);
        Set<StudentGroup> groups = Sets.newHashSet(group);
        groups.add(group);
        groups.add(group2);
        Student student = new Student("Kuba", "Baranski", "666", year, null, groups);
        session.save(student);
        UserAccount user1 = new UserAccount("666", "123");
        session.save(user1);
        Grade grade1 = new Grade(new BigDecimal(5), dateTime, student, group);
        Grade grade2 = new Grade(new BigDecimal(5), dateTime, student, group2);
        session.save(grade1);
        session.save(grade2);
        session.save(new Announcement("ogloszenie pierwsze", dateTime, group, teacher));
        session.save(new Announcement("ogloszenie drugie", dateTime, group, teacher));
        session.save(new Announcement("ogloszenie trzecie sprzed 40 dni", dateTime.minusDays(40), group, teacher));
        trans.commit();
        session.close();
    }
}