package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import to2.dziekanat.model.builders.AnnouncementBuilder;
import to2.dziekanat.model.converters.LocalDateTimeDeserializer;
import to2.dziekanat.model.converters.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@announcementid")
public class Announcement {
	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private String text;

	@Column(nullable = false)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime issueDate = LocalDateTime.now();

	@ManyToOne(optional = false)
	@JoinColumn(name = "addressee")
	private StudentGroup addressee;

	@ManyToOne(optional = false)
	@JoinColumn(name = "issuer")
	private Teacher issuer;

	public Announcement() {

	}

	public Announcement(String text, LocalDateTime issueDate, StudentGroup addressee, Teacher issuer) {
		this.text = text;
		this.issueDate = issueDate;
		this.addressee = addressee;
		this.issuer = issuer;
	}

	public static AnnouncementBuilder builder() {
		return new AnnouncementBuilder();
	}

	public long getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public LocalDateTime getIssueDate() {
		return issueDate;
	}

	public StudentGroup getAddressee() {
		return addressee;
	}

	public Teacher getIssuer() {
		return issuer;
	}
}
