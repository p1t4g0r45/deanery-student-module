package to2.dziekanat.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class UserAccount {
    @GeneratedValue
    @Id
    private long id;

    @Column(nullable = false)
    private String studentIdNumber;

    // TEMPORARY!!
    @Column(nullable = false)
    private String password;


    public UserAccount() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStudentIdNumber() {
        return studentIdNumber;
    }

    public void setStudentIdNumber(String studentIdNumber) {
        this.studentIdNumber = studentIdNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserAccount(String studentIdNumber, String password) {
        this.studentIdNumber = studentIdNumber;
        this.password = password;
    }
}
