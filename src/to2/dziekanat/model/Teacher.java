package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import to2.dziekanat.model.builders.TeacherBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@teacherid")
public class Teacher extends Person {
    @Column(nullable = false)
    private String title = "";

    @OneToMany(mappedBy = "supervisor")
    private Set<Course> taughtCourses = new HashSet<>();

    @OneToMany(mappedBy = "groupSupervisor")
    private Set<StudentGroup> taughtGroups = new HashSet<>();

    @OneToMany(mappedBy = "issuer")
    private Set<Announcement> issuedAnnouncements = new HashSet<>();

    @OneToMany(mappedBy = "reviewer")
    private Set<Thesis> reviewedTheses = new HashSet<>();

    @OneToMany(mappedBy = "supervisor")
    private Set<Thesis> supervisedTheses = new HashSet<>();

    public Teacher() {

    }

    public static TeacherBuilder builder() {
        return new TeacherBuilder();
    }

    public Teacher(String firstName, String surname, String title) {
        this.name = firstName;
        this.surname = surname;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public Set<StudentGroup> getTaughtGroups() {
        return taughtGroups;
    }

    public Set<Thesis> getReviewedTheses() {
        return reviewedTheses;
    }

    public Set<Thesis> getSupervisedTheses() {
        return supervisedTheses;
    }

    public Set<Announcement> getIssuedAnnouncements() {
        return issuedAnnouncements;
    }

    public Set<Course> getTaughtCourses() {
        return taughtCourses;
    }

    @Override
    public String getFullName() {
        return title + " " + super.getFullName();
    }
}
