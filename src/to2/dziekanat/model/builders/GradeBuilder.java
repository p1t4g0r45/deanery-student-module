package to2.dziekanat.model.builders;

import to2.dziekanat.model.Grade;
import to2.dziekanat.model.Gradeable;
import to2.dziekanat.model.Student;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class GradeBuilder {
    private BigDecimal score = BigDecimal.ZERO;
    private LocalDateTime gradingTime = LocalDateTime.now();
    private Student gradedStudent;
    private Gradeable gradedSomething;

    public GradeBuilder(Student student, Gradeable gradeable) {
        this.gradedStudent = student;
        this.gradedSomething = gradeable;
    }

    public GradeBuilder withScore(BigDecimal score) {
        this.score = score;
        return this;
    }

    public GradeBuilder withGradingTime(LocalDateTime markingTime) {
        this.gradingTime = markingTime;
        return this;
    }

    private void validateInitializers() {
        boolean hasNoNulls =
                score != null
                        && gradingTime != null
                        && gradedStudent != null
                        && gradedSomething != null;
        if (!hasNoNulls) {
            throw new IllegalStateException("Some of the mandatory fields are null");
        }
    }

    public Grade build() {
        validateInitializers();
        return new Grade(score, gradingTime, gradedStudent, gradedSomething);
    }
}