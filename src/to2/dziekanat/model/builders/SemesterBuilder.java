package to2.dziekanat.model.builders;

import to2.dziekanat.model.Semester;
import to2.dziekanat.model.Year;

import java.time.LocalDate;

public class SemesterBuilder {
    private LocalDate startDate;
    private LocalDate endDate;
    private LocalDate finalGradingDate;
    private Year year;
    private long semesterNumber;

    public SemesterBuilder forYear(Year year) {
        this.year = year;
        return this;
    }

    public SemesterBuilder startingOn(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public SemesterBuilder endingOn(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public SemesterBuilder gradedUntil(LocalDate finalGradingDate) {
        this.finalGradingDate = finalGradingDate;
        return this;
    }

    public SemesterBuilder semesterIndex(long semesterNumber) {
        this.semesterNumber = semesterNumber;
        return this;
    }

    private void validateInitializers() {
        if (startDate == null || endDate == null || finalGradingDate == null) {
            throw new IllegalStateException("Timing information for semester is null");
        }

        if (startDate.isAfter(endDate) || finalGradingDate.isBefore(startDate)) {
            throw new IllegalStateException("Timingg constraints for semester are not kept");
        }

        if (year == null) {
            throw new IllegalStateException("No year supplied for semester");
        }
        if (semesterNumber <= 0) {
            throw new IllegalStateException("Invalid semester number supplied");
        }
    }

    public Semester build() {
        validateInitializers();
        return new Semester(year, semesterNumber, startDate, endDate, finalGradingDate);
    }
}