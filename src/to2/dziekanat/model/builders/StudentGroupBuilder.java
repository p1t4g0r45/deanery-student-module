package to2.dziekanat.model.builders;

import to2.dziekanat.model.ClassesTime;
import to2.dziekanat.model.CourseComponent;
import to2.dziekanat.model.StudentGroup;
import to2.dziekanat.model.Teacher;

import java.util.HashSet;
import java.util.Set;

public class StudentGroupBuilder {
    private CourseComponent masterCourseComponent;
    private Teacher groupSupervisor;
    private String name;
    private Set<ClassesTime> classTimes = new HashSet<>();

    public StudentGroupBuilder withinCourseComponent(CourseComponent masterCourseComponent) {
        this.masterCourseComponent = masterCourseComponent;
        return this;
    }

    public StudentGroupBuilder supervisedBy(Teacher groupSupervisor) {
        this.groupSupervisor = groupSupervisor;
        return this;
    }

    public StudentGroupBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public StudentGroupBuilder withClassesOn(Set<ClassesTime> classTimes) {
        this.classTimes = classTimes;
        return this;
    }

    public StudentGroupBuilder withClassesOn(ClassesTime classTime) {
        this.classTimes = new HashSet<>();
        this.classTimes.add(classTime);
        return this;
    }

    private void validateInitializers() {
        boolean isValid =
                name != null
                        && masterCourseComponent != null
                        && groupSupervisor != null
                        && classTimes != null;

        if (!isValid) {
            throw new IllegalStateException("Incomplete data supplied");
        }
    }

    public StudentGroup build() {
        validateInitializers();
        return new StudentGroup(name, masterCourseComponent, groupSupervisor, classTimes);
    }
}