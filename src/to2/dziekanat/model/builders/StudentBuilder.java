package to2.dziekanat.model.builders;

import to2.dziekanat.model.Student;
import to2.dziekanat.model.StudentGroup;
import to2.dziekanat.model.Thesis;
import to2.dziekanat.model.Year;

import java.util.HashSet;
import java.util.Set;

public class StudentBuilder {
    private String studentIdNumber;
    private String firstName;
    private String surname;
    private Set<Thesis> defendedTheses = new HashSet<>();
    private Set<StudentGroup> attendedGroups = new HashSet<>();
    private Year primaryYear;

    public StudentBuilder onYear(Year year) {
        this.primaryYear = year;
        return this;
    }

    public StudentBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public StudentBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public StudentBuilder withStudentIdNumber(String studentIdNumber) {
        this.studentIdNumber = studentIdNumber;
        return this;
    }

    public StudentBuilder defendingTheses(Set<Thesis> defendedTheses) {
        this.defendedTheses = defendedTheses;
        return this;
    }

    public StudentBuilder attendingGroups(Set<StudentGroup> attendedGroups) {
        this.attendedGroups = attendedGroups;
        return this;
    }

    private void validateInitializers() {
        boolean hasNoNulls =
                firstName != null
                        && surname != null
                        && studentIdNumber != null
                        && primaryYear != null
                        && defendedTheses != null
                        && attendedGroups != null;
        if (!hasNoNulls) {
            throw new IllegalStateException("Some of the mandatory fields are null");
        }
    }

    public Student build() {
        validateInitializers();
        return new Student(firstName, surname, studentIdNumber, primaryYear, defendedTheses, attendedGroups);
    }
}