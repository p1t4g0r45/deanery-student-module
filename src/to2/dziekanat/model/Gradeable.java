package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@gradeableid")
public abstract class Gradeable {
	@Id
	@GeneratedValue
	protected long id;

	@Column(nullable = false)
	@DecimalMin("0.0")
	protected BigDecimal maxScore = BigDecimal.ONE;

	@Column(nullable = false)
	@DecimalMin("0.0")
	protected BigDecimal scoreMultiplier = BigDecimal.ONE;

	@Column(nullable = false)
	protected String scoreType = "";

	@OneToMany(mappedBy = "gradedSomething", cascade = CascadeType.REMOVE)
	@MapKey(name = "gradedStudent")
	protected Map<Student, Grade> grades = new HashMap<>();

	public Optional<Grade> getGradeOfStudent(Student student) {
		return Optional.ofNullable(grades.get(student));
	}

	@JsonIgnore
	public abstract Set<? extends Gradeable> getSubgradables();

	public long getId() {
		return id;
	}

	public Map<Student, Grade> getGrades() {
		return grades;
	}

	public BigDecimal getMaxScore() {
		return maxScore;
	}

	public BigDecimal getScoreMultiplier() {
		return scoreMultiplier;
	}

	public String getScoreType() {
		return scoreType;
	}
}
