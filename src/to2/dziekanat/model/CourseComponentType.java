package to2.dziekanat.model;

import javax.validation.constraints.NotNull;

public enum CourseComponentType {
	LECTURE("lecture"), LAB_CLASS("laboratory class"), SEMINAR("seminar");

	@NotNull
	private String type;

	CourseComponentType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return type;
	}
}
