package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import to2.dziekanat.model.converters.LocalDateTimeDeserializer;
import to2.dziekanat.model.converters.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@feedbackmessageid")
public class FeedbackMessage {
	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime timeSent = LocalDateTime.now();

	@Column(nullable = false)
	private String content = "";

	@ManyToOne(optional = false)
	@JoinColumn(name = "commentedGrade")
	private Grade commentedGrade;

	@ManyToOne(optional = false)
	@JoinColumn(name = "issuer")
	private Person issuer;

	public FeedbackMessage() {

	}

	public FeedbackMessage(Person issuer, Grade grade, String content) {
		this.issuer = issuer;
		this.commentedGrade = grade;
		this.content = content;
	}

	public long getId() {
		return id;
	}

	public LocalDateTime getTimeSent() {
		return timeSent;
	}

	public String getContent() {
		return content;
	}

	public Grade getCommentedGrade() {
		return commentedGrade;
	}

	public Person getIssuer() {
		return issuer;
	}
}
