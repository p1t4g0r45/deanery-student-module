package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import to2.dziekanat.model.builders.CourseBuilder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@courseid")
public class Course extends Gradeable {

	@Column(nullable = false)
	private String courseTitle;

	@ManyToOne(optional = false)
	@JoinColumn(name = "semester")
	private Semester semester;

	@ManyToOne(optional = false)
	@JoinColumn(name = "supervisor")
	private Teacher supervisor;

	@OneToMany(mappedBy = "masterCourse")
	private Set<CourseComponent> courseComponents = new HashSet<>();

	public Course() {

	}

	public Course(String courseTitle, Semester semester, Teacher supervisor) {
		this.courseTitle = courseTitle;
		this.semester = semester;
		this.supervisor = supervisor;
	}

	public static CourseBuilder builder() {
		return new CourseBuilder();
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public Set<CourseComponent> getCourseComponents() {
		return courseComponents;
	}

	public Semester getSemester() {
		return semester;
	}

	public Teacher getSupervisor() {
		return supervisor;
	}

	public Set<Student> getEnrolledStudents() {
		return courseComponents.stream()
				.flatMap(courseComponents -> courseComponents.getEnrolledStudents()
						.stream())
				.collect(Collectors.toSet());
	}

	@Override
	public Set<? extends Gradeable> getSubgradables() {
		return courseComponents;
	}
}
