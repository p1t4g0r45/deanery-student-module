package to2.dziekanat.view.login;

import com.vaadin.ui.*;
import to2.dziekanat.util.GUI;

public class LogInView extends Panel {

    public LogInView(LoginListener listener) {
        TextField username = GUI.textfield().width("100%").caption("Indeks").get();
        TextField password = GUI.textfield().width("100%").caption("Haslo").get();
        Button loginButton = GUI.button().caption("Zaloguj").onClick(event -> listener.onLogin(username.getValue(), password.getValue())).get();
        VerticalLayout content = GUI.vertical().height("180px").width("180px")
                .addComponent(username)
                .addComponent(password)
                .addComponent(loginButton).align(loginButton, Alignment.BOTTOM_CENTER)
                .get();

        setSizeUndefined();
        setHeight("200px");
        setWidth("200px");
        setContent(content);
        addStyleName("loginPanel");
    }
}
