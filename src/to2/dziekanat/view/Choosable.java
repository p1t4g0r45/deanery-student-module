package to2.dziekanat.view;

import com.vaadin.ui.Panel;

public abstract class Choosable extends Panel {
    public abstract void handleMenuSelectionChange();
}
