package to2.dziekanat.view.marks;

import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import to2.dziekanat.model.Course;
import to2.dziekanat.model.Semester;
import to2.dziekanat.model.Student;
import to2.dziekanat.persistence.HibernateUtils;
import to2.dziekanat.util.GUI;
import to2.dziekanat.util.Style;
import to2.dziekanat.view.Choosable;

import java.util.List;

public class MarksView extends Choosable {

    private static final String SEMESTER_MESSAGE = "Wybierz semestr";
    private static final String SUBJECT_MESSAGE = "Wybierz przedmiot";
    private MarksTable table;
    private ComboBox semesters;
    private ComboBox subjects;

    public MarksView() {
        setContent(createContent());
    }

    private Component createContent() {
    	
    	Student user = (Student) UI.getCurrent().getSession().getAttribute("user");
    	
        semesters = GUI.combobox().nullSelection(false).onValueChange(event -> reloadSubjects()).width("200px")
                .addItem(SEMESTER_MESSAGE).value(SEMESTER_MESSAGE)
                .get();
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            getSemestersForUser(user, session).stream().forEach(s -> {
                semesters.addItem(s);
                semesters.setItemCaption(s, String.valueOf(s.getSemesterNumber()));
            });
        }
        
        subjects = GUI.combobox().nullSelection(false).onValueChange(event -> reloadTable()).width("200px")
                .addItem(SUBJECT_MESSAGE).value(SUBJECT_MESSAGE)
                .enabled(false)
                .get();
        
        table = new MarksTable();
        
        return GUI.vertical().padding().styles(Style.DEFAULT_BGCOLOUR)
                .addComponent(semesters)
                .addComponent(subjects)
                .addComponent(table)
                .get();
        
    }

    private void reloadSubjects() {
        if (semesters == null || subjects == null) {
            return;
        }
        Object semester = semesters.getValue();
        table.removeAllItems();
        subjects.removeAllItems();
        subjects.addItem(SUBJECT_MESSAGE);
        subjects.setValue(SUBJECT_MESSAGE);
        subjects.setEnabled(semester != SEMESTER_MESSAGE);
        if (semester != SEMESTER_MESSAGE) {
            try (Session session = HibernateUtils.getSessionFactory().openSession()) {
                for (Course subject : getSubjectsForSemester((Semester) semester, session)) {
                    subjects.addItem(subject);
                    subjects.setItemCaption(subject, subject.getCourseTitle());
                }
            }
        }
    }
    
    private void reloadTable() {
        if (semesters == null || subjects == null) {
            return;
        }
        Object semester = semesters.getValue();
        Object subject = subjects.getValue();
        if (semester == SEMESTER_MESSAGE || subject == SUBJECT_MESSAGE || subject == null) {
            table.removeAllItems();
        } else {
            try (Session session = HibernateUtils.getSessionFactory().openSession()) {
                table.loadSemester((Semester) semester, (Course) subject, session);
            }
        }
    }

    @Override
    public void handleMenuSelectionChange() {

    }

    private List<Semester> getSemestersForUser(Student user, Session session) {
        return session.createCriteria(Semester.class, "semester")
        		.createAlias("semester.courses", "course")
        		.createAlias("course.courseComponents", "courseComponent")
        		.createAlias("courseComponent.studentGroups", "studentGroup")
        		.createAlias("studentGroup.students", "student")
        		.add(Restrictions.eq("student.id", user.getId())).list();
    }
    
    private List<Course> getSubjectsForSemester(Semester semester, Session session) {
    	return session.createCriteria(Course.class).add(Restrictions.eq("semester", semester)).list();
    }

}
